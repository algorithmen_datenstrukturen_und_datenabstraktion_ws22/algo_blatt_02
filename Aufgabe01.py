def addieren(p1, p2):
    p3 = dict(p1)
    for i, a in p2.items():
        p3[i] = p3.get(i, 0) + a
    return p3

def multiplizieren(p1, p2):
    p3 = {}
    for i, a in p1:
        for j, b in p2:
            p3[i+j] = p3.get(i*j, 0) + a * b
    return p3

def auswerten(p1, x):
    return sum([a * x**i for i, a in p1.items()])
