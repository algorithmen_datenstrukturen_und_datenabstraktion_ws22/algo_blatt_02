def HeapSort(array):
    '''Sorts the array by using a max-heap.

    Parameters
    ----------
    array: list[int]
        a list containing the integers to be sorted

    Returns
    -------
    sorted_array: a sorted copy of the array
    '''

    p = lambda i: (i-1)//2
    l = lambda i: 2*i + 1
    r = lambda i: 2*i + 2

    sorted_array = list(array)

    # Erzeuge einen max-Heap
    for i in range(1, len(sorted_array)):
        j = i
        while sorted_array[j] > sorted_array[p(j)] and j != 0:
            sorted_array[j], sorted_array[p(j)] = sorted_array[p(j)], sorted_array[j]
            j = p(j)

    # Entferne max aus dem max-Heap
    for i in range(len(sorted_array)-1, 0, -1):
        sorted_array[0], sorted_array[i] = sorted_array[i], sorted_array[0]
        j = 0
        while True:
            if l(j) >= i or r(j) >= i:
                break

            if r(j) >= i and sorted_array[l(j)] > sorted_array[j]:
                sorted_array[j], sorted_array[l(j)] = sorted_array[l(j)], sorted_array[j]
                j = l(j)

            if sorted_array[j] >= sorted_array[l(j)] and sorted_array[j] >= sorted_array[r(j)]:
                break

            if sorted_array[l(j)] > sorted_array[r(j)]:
                sorted_array[j], sorted_array[l(j)] = sorted_array[l(j)], sorted_array[j]
                j = l(j)
            else:
                sorted_array[j], sorted_array[r(j)] = sorted_array[r(j)], sorted_array[j]
                j = r(j)

    return sorted_array

def InsertionSort(array):
    '''Sorts the array by inserting each element into an already sorted part of the array.

    Parameters
    ----------
    array: list[int]
        a list containing the intergers to be sorted

    Returns
    -------
    sorted_array: a sorted copy of the array
    '''

    sorted_array = list(array)

    for i in range(1, len(array)):
        for j in range(i, 0, -1):
            if sorted_array[j] < sorted_array[j-1]:
                sorted_array[j], sorted_array[j-1] = sorted_array[j-1], sorted_array[j]
            else:
                break

    return sorted_array

def CountingSort(array, k=None):
    '''Sorts the array by counting the occurrence of each element in the array.

    Parameters
    ----------
    array: list[int]
        a list containing the (positive) intergers to be sorted

    Returns
    -------
    sorted_array: a sorted copy of the array
    '''

    if k is None:
        k = max(array)

    key_array = [0]*(k+1)

    # Zählen der Elemente
    for i in array:
        key_array[i] += 1

    sorted_array = [0]*len(array)

    i = 0
    for j, k in enumerate(key_array):
        while k > 0:
            sorted_array[i] = j
            i += 1
            k -= 1

    return sorted_array
